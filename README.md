# Treesitter parser for Hoi4 localisation files

Since paradox "yaml" files aren't yaml files at all barring some surface-level similarities (for instance,
the game engine accepts unescaped quotes in the middle of a localisation
value), I created this parser to hopefully bring decent syntax highlighting to
neovim users.

It mostly works, but certain things like newlines don't get any special
highlighting. Also, you will have to manually install it into your text editor
of choice. I'll probably make a neovim plugin that makes it painless, though.

The sample loc file belongs to the Equestria at War team. Everything else is
published under the unlicense.
