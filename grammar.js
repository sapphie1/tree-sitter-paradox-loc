module.exports = grammar({
    name: 'paradox_loc',

    rules: {
        loc_file: $ => seq($.language_declaration, repeat(choice($.loc_entry, $.eol))),
        language_declaration: $ => seq("l_", $.language, ":", $.eol),
        loc_entry: $ => seq($.loc_key, $.loc_col, optional($.loc_ver), $.loc_str, $.eol),
        loc_ver: $ => /\d+/,
        loc_col: $ => ":",
        language: $ => choice("english", "french", "german", "spanish", "braz_por", "polish", "russian", "japanese", "simp_chinese"),
        eol: $ => choice($.comment, '\n'),
        comment: $ => /#[^\n]*\n/,
        loc_key: $ => /[\w_\-\.]+/,
    },
    externals: $ => [$.loc_str],
    inline: $ => [$.eol]
});
