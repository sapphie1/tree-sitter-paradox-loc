import fileinput
import re

line_re = re.compile(r"\[(?P<line>\d+), \d+\]")
for line in fileinput.input():
    a = line.find("loc_entry")
    if a >= 0:
        match = line_re.findall(line)
        print(line, end='')
        if match:
            m = match
            assert m[0] == m[1]
        else:
            print("snatehusntheusnh")
