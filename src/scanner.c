#include "tree_sitter/parser.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

enum TokenType {
  LOC_STR,
};

static inline void advance(TSLexer *lexer) { lexer->advance(lexer, false); }

static inline void skip(TSLexer *lexer) { lexer->advance(lexer, true); }

#ifdef DEBUG_ADV
#define advance(lexer)                                                         \
  {                                                                            \
    printf("advance %c, L%d\n", lexer->lookahead, __LINE__);                   \
    advance(lexer);                                                            \
  }

#define skip(lexer)                                                            \
  {                                                                            \
    printf("skip %c, L%d\n", lexer->lookahead, __LINE__);                      \
    skip(lexer);                                                               \
  }
#endif

bool is_space(int32_t c) {
  switch (c) {
  case ' ':
  case '\f':
  case '\r':
  case '\v':
  case '\t':
    return true;
  default:
    return false;
  }
}

// Advance until the end of line but don't consume any tokens
bool advance_till_eol(TSLexer *lexer) {
  while (true) {
    if (lexer->lookahead == '\n' || lexer->lookahead == '#' ||
        lexer->eof(lexer)) {
      advance(lexer);
      lexer->result_symbol = LOC_STR;
      return true;
    }

    if (!is_space(lexer->lookahead)) {
      return false;
    }
    advance(lexer);
  }
}

// Parses a proper ("closed") string until it encounters the closing quote,
// at which point it returns `true`. Returns `false` if an end of line or file
// was encountered prior
bool parse_closed_string(TSLexer *lexer) {
  while (!lexer->eof(lexer)) {
    if (lexer->lookahead == '"') {
      advance(lexer);
      return true;
    }

    // Newline before quote: derp!
    if (lexer->lookahead == '\n') {
      return false;
    }

    advance(lexer);
  }

  return false;
}

void *tree_sitter_paradox_loc_external_scanner_create(void) { return NULL; }

void tree_sitter_paradox_loc_external_scanner_destroy(void *payload) {}

bool tree_sitter_paradox_loc_external_scanner_scan(void *payload,
                                                   TSLexer *lexer,
                                                   const bool *valid_symbols) {
  while (true) {
    int32_t c = lexer->lookahead;
    if (lexer->eof(lexer)) {
      return false;
    }
    if (!is_space(c)) {
      if (c != '"') {
        return false;
      }
      break;
    }
    skip(lexer);
  }
  advance(lexer);

  while (true) {
    if (!parse_closed_string(lexer)) {
      return false;
    }
    lexer->mark_end(lexer);
    if (advance_till_eol(lexer)) {
      return true;
    }
  }
  return false;
}

unsigned tree_sitter_paradox_loc_external_scanner_serialize(void *payload,
                                                            char *buffer) {
  return 0;
}

void tree_sitter_paradox_loc_external_scanner_deserialize(void *payload,
                                                          const char *buffer,
                                                          unsigned length) {}
