#include "tree_sitter/parser.h"

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 19
#define LARGE_STATE_COUNT 2
#define SYMBOL_COUNT 23
#define ALIAS_COUNT 0
#define TOKEN_COUNT 17
#define EXTERNAL_TOKEN_COUNT 1
#define FIELD_COUNT 0
#define MAX_ALIAS_SEQUENCE_LENGTH 5
#define PRODUCTION_ID_COUNT 1

enum ts_symbol_identifiers {
  anon_sym_l_ = 1,
  anon_sym_COLON = 2,
  sym_loc_ver = 3,
  anon_sym_english = 4,
  anon_sym_french = 5,
  anon_sym_german = 6,
  anon_sym_spanish = 7,
  anon_sym_braz_por = 8,
  anon_sym_polish = 9,
  anon_sym_russian = 10,
  anon_sym_japanese = 11,
  anon_sym_simp_chinese = 12,
  anon_sym_LF = 13,
  sym_comment = 14,
  sym_loc_key = 15,
  sym_loc_str = 16,
  sym_loc_file = 17,
  sym_language_declaration = 18,
  sym_loc_entry = 19,
  sym_loc_col = 20,
  sym_language = 21,
  aux_sym_loc_file_repeat1 = 22,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [anon_sym_l_] = "l_",
  [anon_sym_COLON] = ":",
  [sym_loc_ver] = "loc_ver",
  [anon_sym_english] = "english",
  [anon_sym_french] = "french",
  [anon_sym_german] = "german",
  [anon_sym_spanish] = "spanish",
  [anon_sym_braz_por] = "braz_por",
  [anon_sym_polish] = "polish",
  [anon_sym_russian] = "russian",
  [anon_sym_japanese] = "japanese",
  [anon_sym_simp_chinese] = "simp_chinese",
  [anon_sym_LF] = "\n",
  [sym_comment] = "comment",
  [sym_loc_key] = "loc_key",
  [sym_loc_str] = "loc_str",
  [sym_loc_file] = "loc_file",
  [sym_language_declaration] = "language_declaration",
  [sym_loc_entry] = "loc_entry",
  [sym_loc_col] = "loc_col",
  [sym_language] = "language",
  [aux_sym_loc_file_repeat1] = "loc_file_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [anon_sym_l_] = anon_sym_l_,
  [anon_sym_COLON] = anon_sym_COLON,
  [sym_loc_ver] = sym_loc_ver,
  [anon_sym_english] = anon_sym_english,
  [anon_sym_french] = anon_sym_french,
  [anon_sym_german] = anon_sym_german,
  [anon_sym_spanish] = anon_sym_spanish,
  [anon_sym_braz_por] = anon_sym_braz_por,
  [anon_sym_polish] = anon_sym_polish,
  [anon_sym_russian] = anon_sym_russian,
  [anon_sym_japanese] = anon_sym_japanese,
  [anon_sym_simp_chinese] = anon_sym_simp_chinese,
  [anon_sym_LF] = anon_sym_LF,
  [sym_comment] = sym_comment,
  [sym_loc_key] = sym_loc_key,
  [sym_loc_str] = sym_loc_str,
  [sym_loc_file] = sym_loc_file,
  [sym_language_declaration] = sym_language_declaration,
  [sym_loc_entry] = sym_loc_entry,
  [sym_loc_col] = sym_loc_col,
  [sym_language] = sym_language,
  [aux_sym_loc_file_repeat1] = aux_sym_loc_file_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_l_] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COLON] = {
    .visible = true,
    .named = false,
  },
  [sym_loc_ver] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_english] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_french] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_german] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_spanish] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_braz_por] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_polish] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_russian] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_japanese] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_simp_chinese] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LF] = {
    .visible = true,
    .named = false,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_loc_key] = {
    .visible = true,
    .named = true,
  },
  [sym_loc_str] = {
    .visible = true,
    .named = true,
  },
  [sym_loc_file] = {
    .visible = true,
    .named = true,
  },
  [sym_language_declaration] = {
    .visible = true,
    .named = true,
  },
  [sym_loc_entry] = {
    .visible = true,
    .named = true,
  },
  [sym_loc_col] = {
    .visible = true,
    .named = true,
  },
  [sym_language] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_loc_file_repeat1] = {
    .visible = false,
    .named = false,
  },
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 7,
  [8] = 8,
  [9] = 9,
  [10] = 10,
  [11] = 11,
  [12] = 12,
  [13] = 13,
  [14] = 14,
  [15] = 15,
  [16] = 16,
  [17] = 17,
  [18] = 18,
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(61);
      if (lookahead == '#') ADVANCE(1);
      if (lookahead == ':') ADVANCE(63);
      if (lookahead == 'b') ADVANCE(49);
      if (lookahead == 'e') ADVANCE(35);
      if (lookahead == 'f') ADVANCE(48);
      if (lookahead == 'g') ADVANCE(16);
      if (lookahead == 'j') ADVANCE(5);
      if (lookahead == 'l') ADVANCE(2);
      if (lookahead == 'p') ADVANCE(42);
      if (lookahead == 'r') ADVANCE(58);
      if (lookahead == 's') ADVANCE(25);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(0)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(64);
      END_STATE();
    case 1:
      if (lookahead == '\n') ADVANCE(75);
      if (lookahead != 0) ADVANCE(1);
      END_STATE();
    case 2:
      if (lookahead == '_') ADVANCE(62);
      END_STATE();
    case 3:
      if (lookahead == '_') ADVANCE(44);
      END_STATE();
    case 4:
      if (lookahead == '_') ADVANCE(12);
      END_STATE();
    case 5:
      if (lookahead == 'a') ADVANCE(46);
      END_STATE();
    case 6:
      if (lookahead == 'a') ADVANCE(59);
      END_STATE();
    case 7:
      if (lookahead == 'a') ADVANCE(41);
      END_STATE();
    case 8:
      if (lookahead == 'a') ADVANCE(39);
      END_STATE();
    case 9:
      if (lookahead == 'a') ADVANCE(37);
      END_STATE();
    case 10:
      if (lookahead == 'a') ADVANCE(38);
      END_STATE();
    case 11:
      if (lookahead == 'c') ADVANCE(20);
      END_STATE();
    case 12:
      if (lookahead == 'c') ADVANCE(24);
      END_STATE();
    case 13:
      if (lookahead == 'e') ADVANCE(72);
      END_STATE();
    case 14:
      if (lookahead == 'e') ADVANCE(73);
      END_STATE();
    case 15:
      if (lookahead == 'e') ADVANCE(36);
      END_STATE();
    case 16:
      if (lookahead == 'e') ADVANCE(50);
      END_STATE();
    case 17:
      if (lookahead == 'e') ADVANCE(55);
      END_STATE();
    case 18:
      if (lookahead == 'e') ADVANCE(56);
      END_STATE();
    case 19:
      if (lookahead == 'g') ADVANCE(32);
      END_STATE();
    case 20:
      if (lookahead == 'h') ADVANCE(66);
      END_STATE();
    case 21:
      if (lookahead == 'h') ADVANCE(70);
      END_STATE();
    case 22:
      if (lookahead == 'h') ADVANCE(65);
      END_STATE();
    case 23:
      if (lookahead == 'h') ADVANCE(68);
      END_STATE();
    case 24:
      if (lookahead == 'h') ADVANCE(30);
      END_STATE();
    case 25:
      if (lookahead == 'i') ADVANCE(33);
      if (lookahead == 'p') ADVANCE(7);
      END_STATE();
    case 26:
      if (lookahead == 'i') ADVANCE(53);
      END_STATE();
    case 27:
      if (lookahead == 'i') ADVANCE(54);
      END_STATE();
    case 28:
      if (lookahead == 'i') ADVANCE(57);
      END_STATE();
    case 29:
      if (lookahead == 'i') ADVANCE(10);
      END_STATE();
    case 30:
      if (lookahead == 'i') ADVANCE(40);
      END_STATE();
    case 31:
      if (lookahead == 'l') ADVANCE(26);
      END_STATE();
    case 32:
      if (lookahead == 'l') ADVANCE(27);
      END_STATE();
    case 33:
      if (lookahead == 'm') ADVANCE(45);
      END_STATE();
    case 34:
      if (lookahead == 'm') ADVANCE(9);
      END_STATE();
    case 35:
      if (lookahead == 'n') ADVANCE(19);
      END_STATE();
    case 36:
      if (lookahead == 'n') ADVANCE(11);
      END_STATE();
    case 37:
      if (lookahead == 'n') ADVANCE(67);
      END_STATE();
    case 38:
      if (lookahead == 'n') ADVANCE(71);
      END_STATE();
    case 39:
      if (lookahead == 'n') ADVANCE(17);
      END_STATE();
    case 40:
      if (lookahead == 'n') ADVANCE(18);
      END_STATE();
    case 41:
      if (lookahead == 'n') ADVANCE(28);
      END_STATE();
    case 42:
      if (lookahead == 'o') ADVANCE(31);
      END_STATE();
    case 43:
      if (lookahead == 'o') ADVANCE(47);
      END_STATE();
    case 44:
      if (lookahead == 'p') ADVANCE(43);
      END_STATE();
    case 45:
      if (lookahead == 'p') ADVANCE(4);
      END_STATE();
    case 46:
      if (lookahead == 'p') ADVANCE(8);
      END_STATE();
    case 47:
      if (lookahead == 'r') ADVANCE(69);
      END_STATE();
    case 48:
      if (lookahead == 'r') ADVANCE(15);
      END_STATE();
    case 49:
      if (lookahead == 'r') ADVANCE(6);
      END_STATE();
    case 50:
      if (lookahead == 'r') ADVANCE(34);
      END_STATE();
    case 51:
      if (lookahead == 's') ADVANCE(52);
      END_STATE();
    case 52:
      if (lookahead == 's') ADVANCE(29);
      END_STATE();
    case 53:
      if (lookahead == 's') ADVANCE(21);
      END_STATE();
    case 54:
      if (lookahead == 's') ADVANCE(22);
      END_STATE();
    case 55:
      if (lookahead == 's') ADVANCE(13);
      END_STATE();
    case 56:
      if (lookahead == 's') ADVANCE(14);
      END_STATE();
    case 57:
      if (lookahead == 's') ADVANCE(23);
      END_STATE();
    case 58:
      if (lookahead == 'u') ADVANCE(51);
      END_STATE();
    case 59:
      if (lookahead == 'z') ADVANCE(3);
      END_STATE();
    case 60:
      if (eof) ADVANCE(61);
      if (lookahead == '\n') ADVANCE(74);
      if (lookahead == '#') ADVANCE(1);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(60)
      if (lookahead == '-' ||
          lookahead == '.' ||
          ('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(76);
      END_STATE();
    case 61:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 62:
      ACCEPT_TOKEN(anon_sym_l_);
      END_STATE();
    case 63:
      ACCEPT_TOKEN(anon_sym_COLON);
      END_STATE();
    case 64:
      ACCEPT_TOKEN(sym_loc_ver);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(64);
      END_STATE();
    case 65:
      ACCEPT_TOKEN(anon_sym_english);
      END_STATE();
    case 66:
      ACCEPT_TOKEN(anon_sym_french);
      END_STATE();
    case 67:
      ACCEPT_TOKEN(anon_sym_german);
      END_STATE();
    case 68:
      ACCEPT_TOKEN(anon_sym_spanish);
      END_STATE();
    case 69:
      ACCEPT_TOKEN(anon_sym_braz_por);
      END_STATE();
    case 70:
      ACCEPT_TOKEN(anon_sym_polish);
      END_STATE();
    case 71:
      ACCEPT_TOKEN(anon_sym_russian);
      END_STATE();
    case 72:
      ACCEPT_TOKEN(anon_sym_japanese);
      END_STATE();
    case 73:
      ACCEPT_TOKEN(anon_sym_simp_chinese);
      END_STATE();
    case 74:
      ACCEPT_TOKEN(anon_sym_LF);
      if (lookahead == '\n') ADVANCE(74);
      END_STATE();
    case 75:
      ACCEPT_TOKEN(sym_comment);
      END_STATE();
    case 76:
      ACCEPT_TOKEN(sym_loc_key);
      if (lookahead == '-' ||
          lookahead == '.' ||
          ('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(76);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0, .external_lex_state = 1},
  [1] = {.lex_state = 0},
  [2] = {.lex_state = 0},
  [3] = {.lex_state = 60},
  [4] = {.lex_state = 60},
  [5] = {.lex_state = 60},
  [6] = {.lex_state = 60},
  [7] = {.lex_state = 60},
  [8] = {.lex_state = 60},
  [9] = {.lex_state = 0},
  [10] = {.lex_state = 60},
  [11] = {.lex_state = 0, .external_lex_state = 1},
  [12] = {.lex_state = 0, .external_lex_state = 1},
  [13] = {.lex_state = 60},
  [14] = {.lex_state = 60},
  [15] = {.lex_state = 0},
  [16] = {.lex_state = 0},
  [17] = {.lex_state = 0},
  [18] = {.lex_state = 0, .external_lex_state = 1},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [anon_sym_l_] = ACTIONS(1),
    [anon_sym_COLON] = ACTIONS(1),
    [sym_loc_ver] = ACTIONS(1),
    [anon_sym_english] = ACTIONS(1),
    [anon_sym_french] = ACTIONS(1),
    [anon_sym_german] = ACTIONS(1),
    [anon_sym_spanish] = ACTIONS(1),
    [anon_sym_braz_por] = ACTIONS(1),
    [anon_sym_polish] = ACTIONS(1),
    [anon_sym_russian] = ACTIONS(1),
    [anon_sym_japanese] = ACTIONS(1),
    [anon_sym_simp_chinese] = ACTIONS(1),
    [sym_comment] = ACTIONS(1),
    [sym_loc_str] = ACTIONS(1),
  },
  [1] = {
    [sym_loc_file] = STATE(15),
    [sym_language_declaration] = STATE(3),
    [anon_sym_l_] = ACTIONS(3),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 2,
    STATE(17), 1,
      sym_language,
    ACTIONS(5), 9,
      anon_sym_english,
      anon_sym_french,
      anon_sym_german,
      anon_sym_spanish,
      anon_sym_braz_por,
      anon_sym_polish,
      anon_sym_russian,
      anon_sym_japanese,
      anon_sym_simp_chinese,
  [15] = 5,
    ACTIONS(7), 1,
      ts_builtin_sym_end,
    ACTIONS(9), 1,
      anon_sym_LF,
    ACTIONS(11), 1,
      sym_comment,
    ACTIONS(13), 1,
      sym_loc_key,
    STATE(4), 2,
      sym_loc_entry,
      aux_sym_loc_file_repeat1,
  [32] = 5,
    ACTIONS(13), 1,
      sym_loc_key,
    ACTIONS(15), 1,
      ts_builtin_sym_end,
    ACTIONS(17), 1,
      anon_sym_LF,
    ACTIONS(19), 1,
      sym_comment,
    STATE(5), 2,
      sym_loc_entry,
      aux_sym_loc_file_repeat1,
  [49] = 5,
    ACTIONS(21), 1,
      ts_builtin_sym_end,
    ACTIONS(23), 1,
      anon_sym_LF,
    ACTIONS(26), 1,
      sym_comment,
    ACTIONS(29), 1,
      sym_loc_key,
    STATE(5), 2,
      sym_loc_entry,
      aux_sym_loc_file_repeat1,
  [66] = 2,
    ACTIONS(32), 2,
      ts_builtin_sym_end,
      anon_sym_LF,
    ACTIONS(34), 2,
      sym_comment,
      sym_loc_key,
  [75] = 2,
    ACTIONS(36), 2,
      ts_builtin_sym_end,
      anon_sym_LF,
    ACTIONS(38), 2,
      sym_comment,
      sym_loc_key,
  [84] = 2,
    ACTIONS(40), 2,
      ts_builtin_sym_end,
      anon_sym_LF,
    ACTIONS(42), 2,
      sym_comment,
      sym_loc_key,
  [93] = 2,
    ACTIONS(44), 1,
      anon_sym_COLON,
    STATE(12), 1,
      sym_loc_col,
  [100] = 2,
    ACTIONS(46), 1,
      anon_sym_LF,
    ACTIONS(48), 1,
      sym_comment,
  [107] = 1,
    ACTIONS(50), 2,
      sym_loc_str,
      sym_loc_ver,
  [112] = 2,
    ACTIONS(52), 1,
      sym_loc_ver,
    ACTIONS(54), 1,
      sym_loc_str,
  [119] = 2,
    ACTIONS(56), 1,
      anon_sym_LF,
    ACTIONS(58), 1,
      sym_comment,
  [126] = 2,
    ACTIONS(60), 1,
      anon_sym_LF,
    ACTIONS(62), 1,
      sym_comment,
  [133] = 1,
    ACTIONS(64), 1,
      ts_builtin_sym_end,
  [137] = 1,
    ACTIONS(66), 1,
      anon_sym_COLON,
  [141] = 1,
    ACTIONS(68), 1,
      anon_sym_COLON,
  [145] = 1,
    ACTIONS(70), 1,
      sym_loc_str,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(2)] = 0,
  [SMALL_STATE(3)] = 15,
  [SMALL_STATE(4)] = 32,
  [SMALL_STATE(5)] = 49,
  [SMALL_STATE(6)] = 66,
  [SMALL_STATE(7)] = 75,
  [SMALL_STATE(8)] = 84,
  [SMALL_STATE(9)] = 93,
  [SMALL_STATE(10)] = 100,
  [SMALL_STATE(11)] = 107,
  [SMALL_STATE(12)] = 112,
  [SMALL_STATE(13)] = 119,
  [SMALL_STATE(14)] = 126,
  [SMALL_STATE(15)] = 133,
  [SMALL_STATE(16)] = 137,
  [SMALL_STATE(17)] = 141,
  [SMALL_STATE(18)] = 145,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, SHIFT(2),
  [5] = {.entry = {.count = 1, .reusable = true}}, SHIFT(16),
  [7] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_loc_file, 1),
  [9] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [11] = {.entry = {.count = 1, .reusable = false}}, SHIFT(4),
  [13] = {.entry = {.count = 1, .reusable = false}}, SHIFT(9),
  [15] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_loc_file, 2),
  [17] = {.entry = {.count = 1, .reusable = true}}, SHIFT(5),
  [19] = {.entry = {.count = 1, .reusable = false}}, SHIFT(5),
  [21] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_loc_file_repeat1, 2),
  [23] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_loc_file_repeat1, 2), SHIFT_REPEAT(5),
  [26] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_loc_file_repeat1, 2), SHIFT_REPEAT(5),
  [29] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_loc_file_repeat1, 2), SHIFT_REPEAT(9),
  [32] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_language_declaration, 4),
  [34] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_language_declaration, 4),
  [36] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_loc_entry, 4),
  [38] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_loc_entry, 4),
  [40] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_loc_entry, 5),
  [42] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_loc_entry, 5),
  [44] = {.entry = {.count = 1, .reusable = true}}, SHIFT(11),
  [46] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [48] = {.entry = {.count = 1, .reusable = false}}, SHIFT(6),
  [50] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_loc_col, 1),
  [52] = {.entry = {.count = 1, .reusable = true}}, SHIFT(18),
  [54] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [56] = {.entry = {.count = 1, .reusable = true}}, SHIFT(7),
  [58] = {.entry = {.count = 1, .reusable = false}}, SHIFT(7),
  [60] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [62] = {.entry = {.count = 1, .reusable = false}}, SHIFT(8),
  [64] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [66] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_language, 1),
  [68] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [70] = {.entry = {.count = 1, .reusable = true}}, SHIFT(14),
};

enum ts_external_scanner_symbol_identifiers {
  ts_external_token_loc_str = 0,
};

static const TSSymbol ts_external_scanner_symbol_map[EXTERNAL_TOKEN_COUNT] = {
  [ts_external_token_loc_str] = sym_loc_str,
};

static const bool ts_external_scanner_states[2][EXTERNAL_TOKEN_COUNT] = {
  [1] = {
    [ts_external_token_loc_str] = true,
  },
};

#ifdef __cplusplus
extern "C" {
#endif
void *tree_sitter_paradox_loc_external_scanner_create(void);
void tree_sitter_paradox_loc_external_scanner_destroy(void *);
bool tree_sitter_paradox_loc_external_scanner_scan(void *, TSLexer *, const bool *);
unsigned tree_sitter_paradox_loc_external_scanner_serialize(void *, char *);
void tree_sitter_paradox_loc_external_scanner_deserialize(void *, const char *, unsigned);

#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_paradox_loc(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .external_scanner = {
      &ts_external_scanner_states[0][0],
      ts_external_scanner_symbol_map,
      tree_sitter_paradox_loc_external_scanner_create,
      tree_sitter_paradox_loc_external_scanner_destroy,
      tree_sitter_paradox_loc_external_scanner_scan,
      tree_sitter_paradox_loc_external_scanner_serialize,
      tree_sitter_paradox_loc_external_scanner_deserialize,
    },
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
